package tn.esprit.PIPFE.Services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import tn.esprit.PIPFE.Entities.Admin;
import tn.esprit.PIPFE.Entities.Categorie;
import tn.esprit.PIPFE.Entities.Classe;
import tn.esprit.PIPFE.Entities.Departement;
import tn.esprit.PIPFE.Entities.Ecole;
import tn.esprit.PIPFE.Entities.Encadrant;
import tn.esprit.PIPFE.Entities.EncadrantEntreprise;
import tn.esprit.PIPFE.Entities.Enseignant;
import tn.esprit.PIPFE.Entities.Entreprise;
import tn.esprit.PIPFE.Entities.Etudiant;
import tn.esprit.PIPFE.Entities.FichePFE;
import tn.esprit.PIPFE.Entities.FormulaireConvention;
import tn.esprit.PIPFE.Entities.HeadOfDepartment;
import tn.esprit.PIPFE.Entities.Options;
import tn.esprit.PIPFE.Entities.PreValidateur;
import tn.esprit.PIPFE.Entities.President;
import tn.esprit.PIPFE.Entities.Rapporteur;
import tn.esprit.PIPFE.Entities.Site;
import tn.esprit.PIPFE.Entities.User;



@Stateless
public class CrudServices implements CrudLocal,CrudRemote{

	@PersistenceContext(unitName="ProjetStagePFE-ejb" ,type=PersistenceContextType.TRANSACTION)
	
	EntityManager em;
	
/* -----------------------------------  Formulaire de Convention -------------------------------------------------- */ 
	public void AjouterConvention(FormulaireConvention h) {
		em.persist(h);
		
	}
	@Override
	public FormulaireConvention AfficherConvention(int id) {
		FormulaireConvention f= em.find(FormulaireConvention.class,id);
		return (f);
	}
	@Override
	public void ModifierConvention(FormulaireConvention h) {
		 em.merge(h);
		
	}
	@Override
	public List getAllConvention() {
        return em.createNamedQuery("FormulaireConvention.findAll", FormulaireConvention.class).getResultList();

	}
	@Override
	public void deleteConvention(FormulaireConvention h) {
		if (!em.contains(h)) {
            h = em.merge(h);
        }

        em.remove(h);	
	}
	
	
	
	/* ----------------------------------- Etudiant -------------------------------------------------- */ 

	@Override
	public void AjouterEtudiant(Etudiant h) {
		em.persist(h);	
		em.flush();
		h=em.merge(h);
	}

	@Override
	public Etudiant AfficherEtudiant(int id) {
		Etudiant f= em.find(Etudiant.class,id);
		return (f);
	}

	@Override
	public void ModifierEtudiant(Etudiant h) {
		h=em.merge(h);
		
	}

	@Override
	public List getAllEtudiant() {
        return em.createNamedQuery("Etudiant.findAll", Etudiant.class).getResultList();

	}

	@Override
	public void deleteEtudiant(Etudiant h) {
		if (!em.contains(h)) {
            h = em.merge(h);
        }

        em.remove(h);	
	}
	
	/* ----------------------------------- Classe -------------------------------------------------- */ 

	@Override
	public void AjouterClasse (Classe  h) {
		em.persist(h);		
	}

	@Override
	public Classe  AfficherClasse (int id) {
		Classe  f= em.find(Classe .class,id);
		return (f);
	}

	@Override
	public void ModifierClasse (Classe  h) {
		em.merge(h);
		
	}

	@Override
	public List getAllClasses() {
        return em.createNamedQuery("Classe.findAll", Classe .class).getResultList();

	}

	@Override
	public void deleteClasse (Classe  h) {
		if (!em.contains(h)) {
            h = em.merge(h);
        }

        em.remove(h);	
	}
	
	/* ----------------------------------- Site -------------------------------------------------- */ 
	
	@Override
	 public void AjouterSite (Site h)
	 {
		 em.persist(h);
	 }
	@Override
	 public Site AfficherSite (int id)
	 {
		Site s= em.find(Site.class,id);
		return (s);
		 
	 }
	@Override
	 public void ModifierSite (Site h)
	 {
		 em.merge(h);
		 
	 }
	@Override
	 public List getAllSites() {
		return em.createNamedQuery("Site.findAll", Site.class).getResultList();
	    }
	@Override
	 public void deleteSite(Site h){
	    if (!em.contains(h)) {
	            h = em.merge(h);
	     
	    }

	        em.remove(h);
	    }
	
	
	/* ----------------------------------- Departments -------------------------------------------------- */ 
	
	
	@Override
	public void AjouterDepartement(Departement h) {
		 em.persist(h);		
	}

	@Override
	public Departement AfficherDepartement(int id) {
		Departement s= em.find(Departement.class,id);
		return (s);
	}

	@Override
	public void ModifierDepartement(Departement h) {
		 em.merge(h);
		
	}

	@Override
	public List getAllDepartement() {
		 return em.createNamedQuery("Departement.findAll", Departement.class).getResultList();
	}

	@Override
	public void deleteDepartement(Departement h) {
		 if (!em.contains(h)) {
	            h = em.merge(h);
	        }

	        em.remove(h);
	}
	
	/* ----------------------------------- Options -------------------------------------------------- */ 
	
	@Override
	public void AjouterOptions(Options h) {
		 em.persist(h);		
	}

	@Override
	public Options AfficherOptions(int id) {
		Options s= em.find(Options.class,id);
		return (s);
	}

	@Override
	public void ModifierOptions(Options h) {
		 em.merge(h);
		
	}

	@Override
	public List getAllOptions() {
		 return em.createNamedQuery("Options.findAll", Options.class).getResultList();
	}

	@Override
	public void deleteOptions(Options h) {
		 if (!em.contains(h)) {
	            h = em.merge(h);
	        }

	        em.remove(h);	
	}
	
	@Override
	public void Affecterdepasit(int ids, int idd) {
		Site s=em.find(Site.class, ids);
		Departement d=em.find(Departement.class, idd);
		d.setSite(s);
		
	}

	@Override
	public void Affecteropadep(int idd, int ido) {
		Departement d=em.find(Departement.class, idd);
		Options s=em.find(Options.class, ido);
		s.setDepartement(d);		
	}

	@Override
	public void Affecterclassaop(int ido, int idc) {
		Options s=em.find(Options.class, ido);
		Classe d=em.find(Classe.class, idc);
		d.setOption(s);		
	}
	
	public void Affecteretuaclass(int idc ,int ide){
		Classe c=em.find(Classe.class, idc);
		Etudiant e=em.find(Etudiant.class, ide);
		e.setClasse(c);	
		
	}
	

	
	/* ----------------------------------- Enseigants -------------------------------------------------- */ 	
	@Override
	public void AjouterRapporteur(Rapporteur R) {
		em.persist(R);
		
	}
	@Override
	public void AjouterPreValidateur(PreValidateur P) {
		em.persist(P);
		
	}
	@Override
	public void AjouterEncadrant(Encadrant E) {
		em.persist(E);
		
	}
	@Override
	public void AjouterPresident(President P) {
		em.persist(P);
		
	}
	
	@Override
	public int AjouterEnseignant(Enseignant E) {
		em.persist(E);
		return E.getId();
		
	}
	
	@Override
	public void AffecterCatEncadrant(int idE, int idC) {
		Encadrant ens=em.find(Encadrant.class, idE);
		Categorie cat=em.find(Categorie.class, idC);
		
		ens.getPrefEncadrerrList().add(cat);
		cat.getEncadrants().add(ens);	
	}
	
	@Override
	public void AffecterCatPrevalidateur(int idE, int idC) {
		PreValidateur ens=em.find(PreValidateur.class, idE);
		Categorie cat=em.find(Categorie.class, idC);
		ens.getPrefValiderList().add(cat);
		cat.getPrevalidateurs().add(ens);	
		
	}
	@Override
	public void AffecterEnsAFiche(int idf, int ide) {
		Enseignant cat=em.find(Enseignant.class, ide);
		FichePFE ens=em.find(FichePFE.class, idf);
		
		cat.getFichePFE().add(ens);
		ens.getEnseignant().add(cat);
	}
	
	@Override
	public void AffecterRoleToRapporteur(int Ide) {
		Enseignant e=em.find(Enseignant.class, Ide);
		Rapporteur R= new Rapporteur(e.getCin());
		em.persist(R);
	}
	@Override
	public void AffecterRoleToEncadrant(int Ide) {
		Enseignant e=em.find(Enseignant.class, Ide);
		Encadrant R= new Encadrant(e.getCin());
		em.persist(R);
		
	}
	@Override
	public void AffecterRoleToPreValidateur(int Ide) {
		Enseignant e=em.find(Enseignant.class, Ide);
		PreValidateur R= new PreValidateur(e.getCin());
		em.persist(R);
	}
	@Override
	public void AffecterRoleToPresident(int Ide) {
		Enseignant e=em.find(Enseignant.class, Ide);
		President R= new President(e.getCin());
		em.persist(R);
		
	}
	
	
	/* ----------------------------------- Head of Departments -------------------------------------------------- */ 
	
	/* ----------------------------------- Categories -------------------------------------------------- */ 
	@Override
	public void AjouterCategorie(Categorie c) {
		em.persist(c);
		
	}
	/* ----------------------------------- Directeur de Stages -------------------------------------------------- */
	@Override
	public void AjouterFichePfe(FichePFE f) {
		em.persist(f);
		
	}
	@Override
	public void AffecterEtudiantFiche(int idE, int idF) {
		Etudiant e=em.find(Etudiant.class, idE);
		FichePFE fiche=em.find(FichePFE.class, idF);
		
		fiche.setEtudiant(e);
		e.setFichePfe(fiche);
	}
	
	
	
	
	
	
	
	
	/* ----------------------------------- Ecole -------------------------------------------------- */ 
	
	/* ----------------------------------- Fiche Pfe -------------------------------------------------- */ 
	
	/* ----------------------------------- Soutenance -------------------------------------------------- */ 
	
	
	@Override
	public void AffecterEntrepriseFiche(int idE, int idF) {
		Entreprise e=em.find(Entreprise.class, idE);
		FichePFE fiche=em.find(FichePFE.class, idF);
		
		fiche.setEntreprise(e);
	}
	
	@Override
	public void Affecterencadaent(int ide, int idec) {
		tn.esprit.PIPFE.Entities.Entreprise e=em.find(tn.esprit.PIPFE.Entities.Entreprise.class, ide);
		EncadrantEntreprise f=em.find(EncadrantEntreprise.class, idec);
		f.setEntreprise(e);
		
	}
	
/* ----------------------------------- Entreprise -------------------------------------------------- */ 
	
	
	@Override
	public void AjouterEntreprise(Entreprise e) {
		 em.persist(e);		
		
	}
	@Override
	public Entreprise Entreprise(int id) {
		Entreprise s= em.find(Entreprise.class,id);
		return (s);
	}
	@Override
	public void ModifierEntreprise(Entreprise e) {
		em.merge(e);
		
	}
	@Override
	public List getAllEntreprise() {
		 return em.createNamedQuery("Entreprise.findAll",Entreprise.class).getResultList();

	}
	@Override
	public void deleteEntreprise(Entreprise e) {
		if (!em.contains(e)) {
            e = em.merge(e);
        }

        em.remove(e);	
		
	}
	
/* ----------------------------------- Encadrant-Entreprise ---------------------------------------- */ 
	
	@Override
	public void AjouterEncadrantEntreprise(tn.esprit.PIPFE.Entities.EncadrantEntreprise e) {
		em.persist(e);	
		
	}
	@Override
	public tn.esprit.PIPFE.Entities.EncadrantEntreprise EncadrantEntreprise(int id) {
		tn.esprit.PIPFE.Entities.EncadrantEntreprise s= em.find(tn.esprit.PIPFE.Entities.EncadrantEntreprise.class,id);
		return (s);
	}
	@Override
	public void ModifierEncadrantEntreprise(tn.esprit.PIPFE.Entities.EncadrantEntreprise e) {
		em.merge(e);
		
	}
	@Override
	public List getAllEncadrantEntreprise() {
		 return em.createNamedQuery("EncadrantEntreprise.findAll", tn.esprit.PIPFE.Entities.EncadrantEntreprise.class).getResultList();

	}
	@Override
	public void deleteEncadrantEntreprise(tn.esprit.PIPFE.Entities.EncadrantEntreprise e) {
		if (!em.contains(e)) {
            e = em.merge(e);
        }

        em.remove(e);	
		
	}
	@Override
	public void Affecterformaetu(int ide, int idf) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void Affecterformaent(int ide, int idf) {
		// TODO Auto-generated method stub
		
	}
	
	
	//************ CRUD ECOLED*************

	@Override
	public void ajouterEcole(Ecole a) {
		em.persist(a);	
		em.flush();
		em.merge(a);
			}

			@Override
			public void modifierEcole(Ecole a) {
		a=em.merge(a)		;
			}

			@Override
			public void supprimerEcole(Ecole a) {
				if (!em.contains(a)) {
		            a = em.merge(a);
		        }

		        em.remove(a);
				
			}

			@Override
			public List afficherEcole() {
				return em.createNamedQuery("Ecole.findAll",Ecole.class).getResultList();
			}
			
			public Ecole getEcoleById(int id) {
				Ecole a;
				a=em.find(Ecole.class, id);
				return a ;
				
			}
	
	
			//************CRUD ADMIN*****************
			
			
			@Override
			public int ajouterAdmin(Admin a) {
		em.persist(a);
		return a.getId();
		
		
			}

			@Override
			public void modifierAdmin(Admin a) {
		em.merge(a)		;
			}

			@Override
			public void supprimerAdmin(Admin a) {
				if (!em.contains(a)) {
		            a = em.merge(a);
		        }

		        em.remove(a);
				
			}

			@Override
			public List afficherAdmin() {
				return em.createNamedQuery("Admin.findAll",Admin.class).getResultList();
			}

			@Override
			public void affecterEcoleAdmin(int a, int e) {
				Admin s=em.find(Admin.class, a);
				Ecole d=em.find(Ecole.class, e);
				s.setEcole(d);
			
				
			}
			
			
			public void affecterEcoleSite(int e, int st) {
				Ecole s=em.find(Ecole.class, e);
				Site d=em.find(Site.class, st);
				d.setEcole(s);
			
				
			}
			
			

			@Override
			public Admin getAdminById(int id) {
				Admin a;
				a=em.find(Admin.class, id);
				return a ;
				
			}
			@Override
			public void ajouterUser(User u) {
				em.persist(u);				
			}
			@Override
			public void modifierUser(User u) {
				em.merge(u);
				
			}
			@Override
			public void supprimererUser(User u) {
				if (!em.contains(u)) {
		            u = em.merge(u);
		        }

		        em.remove(u);				
			}
			@Override
			public List afficherUser() {
				return em.createNamedQuery("User.findAll",User.class).getResultList();
				
			}
			@Override
			public User getUserById(int id) {
				User u;
				u=em.find(User.class, id);
				return u ;
			}
			
			public User login(String username , String password) { 
					
				ArrayList<User> liste = (ArrayList<User> )(em.createNamedQuery("User.findAll",User.class).getResultList());
				for(User u : liste) { 
					if((u.getUsername().matches(username)) && (u.getPassword().matches(password)))
						return u;
				}
				return null;
			
			}
			@Override
			public List getAllEnseignant() {
				
				return em.createNamedQuery("Enseignant.findAll",Enseignant.class).getResultList();
				
			}
			@Override
			public List getAllEncadrant() {
				
				return em.createNamedQuery("Encadrant.findAll",Encadrant.class).getResultList();
			}
			@Override
			public List getAllfiche() {
				return em.createNamedQuery("FichePFE.findAll",FichePFE.class).getResultList();
			}
			@Override
			public List getAllchef() {
				return em.createNamedQuery("HeadOfDepartment.findAll",HeadOfDepartment.class).getResultList();
			}
			@Override
			public void ajouterChef(HeadOfDepartment h) {
				em.persist(h);
				
			}
			@Override
			public void modifierEnseignant(Enseignant e) {
				e=em.merge(e);
				
			}
			@Override
			public void deleteEnseignant(Enseignant e) {
				if (!em.contains(e)) {
		            e= em.merge(e);
		        }

		        em.remove(e);	
				
			}
			@Override
			public void affecterEnsDep(int e, int d) {
				Enseignant s=em.find(Enseignant.class, e);
				Departement g=em.find(Departement.class, d);
				s.getDepartements().add(g);
			}
			@Override
			public void affecterDirecteurStage(int ide, int ids) {
				Enseignant s=em.find(Enseignant.class, ide);
				Site g=em.find(Site.class, ids);
				s.setType_employe("DirecteurDeStage");
				
			}
		

			
			
			//***********CRUD SUPERADMIN***********
			
			
			
}
