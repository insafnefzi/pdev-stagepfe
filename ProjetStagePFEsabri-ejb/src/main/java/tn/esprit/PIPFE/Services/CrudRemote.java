package tn.esprit.PIPFE.Services;

import java.text.ChoiceFormat;
import java.util.List;

import javax.ejb.Remote;


import tn.esprit.PIPFE.Entities.Admin;
import tn.esprit.PIPFE.Entities.Categorie;
import tn.esprit.PIPFE.Entities.Classe;
import tn.esprit.PIPFE.Entities.Departement;
import tn.esprit.PIPFE.Entities.DirecteurDeStage;
import tn.esprit.PIPFE.Entities.Ecole;
import tn.esprit.PIPFE.Entities.Encadrant;
import tn.esprit.PIPFE.Entities.Enseignant;
import tn.esprit.PIPFE.Entities.Etudiant;
import tn.esprit.PIPFE.Entities.FichePFE;
import tn.esprit.PIPFE.Entities.FormulaireConvention;
import tn.esprit.PIPFE.Entities.HeadOfDepartment;
import tn.esprit.PIPFE.Entities.Options;
import tn.esprit.PIPFE.Entities.PreValidateur;
import tn.esprit.PIPFE.Entities.President;
import tn.esprit.PIPFE.Entities.Rapporteur;
import tn.esprit.PIPFE.Entities.Site;
import tn.esprit.PIPFE.Entities.User;
import tn.esprit.PIPFE.Entities.Entreprise;


@Remote
public interface CrudRemote {
	
	
	void AjouterConvention (FormulaireConvention h);
	FormulaireConvention AfficherConvention (int id);
	void ModifierConvention (FormulaireConvention h);
	List getAllConvention();
	void deleteConvention(FormulaireConvention h);
	List getAllfiche();
	void AjouterEtudiant (Etudiant h);
	Etudiant AfficherEtudiant (int id);
	void ModifierEtudiant (Etudiant h);
	List getAllEtudiant();
	void deleteEtudiant(Etudiant h);
	List getAllchef();
	public void ajouterChef(HeadOfDepartment h);
	public void affecterEnsDep(int e,int d);
	void AjouterSite (Site h);
	Site AfficherSite (int id);
	void ModifierSite (Site h);
	List getAllSites();
	void deleteSite(Site h);
	public void affecterEcoleSite(int e, int st);
	
	void AjouterDepartement (Departement h);
	Departement AfficherDepartement (int id);
	void ModifierDepartement (Departement h);
	void affecterDirecteurStage(int ide , int ids);
	List getAllDepartement();
	void deleteDepartement(Departement h);
	
	
	void AjouterClasse (Classe h);
	Classe AfficherClasse (int id);
	void ModifierClasse (Classe h);
	List getAllClasses();
	void deleteClasse(Classe h);
	
	void AjouterOptions (Options p);
	Options AfficherOptions (int id);
	void ModifierOptions (Options h);
	List getAllOptions();
	void deleteOptions(Options h);
	
	void Affecterdepasit(int ids ,int idd);
	void Affecteropadep(int idd ,int ido);
	void Affecterclassaop(int ido ,int idc);
	void Affecteretuaclass(int idc ,int ide);
	
	void AjouterRapporteur(Rapporteur rr);
	void AjouterPreValidateur(PreValidateur P);
	void AjouterEncadrant(Encadrant E);
	void AjouterPresident(President P);
	int AjouterEnseignant(Enseignant E);
	
	
	void AffecterCatEncadrant(int idE, int idC);
	void AffecterCatPrevalidateur(int idE, int idC);
	void AffecterEnsAFiche(int idf, int ide);
	void AffecterRoleToRapporteur(int Ide);
	void AffecterRoleToEncadrant(int Ide);
	void AffecterRoleToPreValidateur(int Ide);
	void AffecterRoleToPresident(int Ide);

	void Affecterformaetu(int ide ,int idf);
	void Affecterformaent(int ide ,int idf);
	
	void AjouterCategorie(Categorie c);
	
	void AjouterFichePfe(FichePFE f);
	public void AffecterEtudiantFiche(int idE, int idF);
	
	void AffecterEntrepriseFiche(int idE, int idF);
	 void Affecterencadaent(int ide, int idec);
	 
	 void AjouterEntreprise (Entreprise e);
		Entreprise Entreprise (int id);
		void ModifierEntreprise (Entreprise e);
		List getAllEntreprise();
		void deleteEntreprise(Entreprise e);
		
		
		public List getAllEnseignant() ;
		
		void AjouterEncadrantEntreprise (tn.esprit.PIPFE.Entities.EncadrantEntreprise e);
		tn.esprit.PIPFE.Entities.EncadrantEntreprise EncadrantEntreprise (int id);
		void ModifierEncadrantEntreprise(tn.esprit.PIPFE.Entities.EncadrantEntreprise e);
		List getAllEncadrantEntreprise();
		void deleteEncadrantEntreprise(tn.esprit.PIPFE.Entities.EncadrantEntreprise e);

	//*************** CRUD ADMIN**************
		public int ajouterAdmin(Admin a) ;
		public void modifierAdmin(Admin a) ;
		public void supprimerAdmin(Admin a);
		public List afficherAdmin() ;
		public void affecterEcoleAdmin(int a , int e);
		public Admin getAdminById(int id) ;
		
		
		//*************** CRUD ECOLE**************

		
		public void ajouterEcole(Ecole a) ;
		public void modifierEcole(Ecole a) ;
		public void supprimerEcole(Ecole a);
		public List afficherEcole() ;
		public Ecole getEcoleById(int id);

		//*************** CRUD USER**************
		public void ajouterUser(User u);
		public void modifierUser(User u);
		public void supprimererUser(User u);
		public List afficherUser();
		public User getUserById(int id) ;
		public User login(String username , String password);
		
		//*************** CRUD SUPERADMIN************
		
	
List getAllEncadrant() ;

void modifierEnseignant(Enseignant e);
void deleteEnseignant(Enseignant e);
		
}
