package tn.esprit.PIPFE.Entities;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name = "Etudiant")
@NamedQueries({
    @NamedQuery(name = "Etudiant.findAll", query = "SELECT t FROM Etudiant t")
})
public class Etudiant implements Serializable {

	@Id
	@GeneratedValue
	private int Id;
	private String Nom;
	private String Prenom;
	private int Cin;
	private String EmailProf;
	private String EmailPerso;
	private String Password;
	private int Tel;
	private int Autorise;
	private boolean depot;
	@OneToOne
	private FichePFE FichePfe;
	@OneToOne
	private FormulaireConvention FormulaireConvention;
	@ManyToOne
	private Classe classe;

	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	
	
	
	
	public boolean getDepot() {
		return depot;
	}
	public void setDepot(boolean depot) {
		this.depot = depot;
	}
	public int getAutorise() {
		return Autorise;
	}
	public void setAutorise(int autorise) {
		Autorise = autorise;
	}

	public FichePFE getFichePfe() {
		return FichePfe;
	}
	public void setFichePfe(FichePFE fichePfe) {
		FichePfe = fichePfe;
	}

	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	
	
	
	
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public int getTel() {
		return Tel;
	}
	public void setTel(int tel) {
		Tel = tel;
	}
	public Etudiant() {
		super();
	}

	public Etudiant(String nom, String prenom) {
		super();
		Nom = nom;
		Prenom = prenom;
	}

	public FormulaireConvention getFormulaireConvention() {
		return FormulaireConvention;
	}
	public void setFormulaireConvention(FormulaireConvention formulaireConvention) {
		FormulaireConvention = formulaireConvention;
	}
	public int getCin() {
		return Cin;
	}
	public void setCin(int cin) {
		Cin = cin;
	}
	public String getEmailProf() {
		return EmailProf;
	}
	public void setEmailProf(String emailProf) {
		EmailProf = emailProf;
	}
	public String getEmailPerso() {
		return EmailPerso;
	}
	public void setEmailPerso(String emailPerso) {
		EmailPerso = emailPerso;
	}
	@Override
	public String toString() {
		return "Etudiant [Id=" + Id + ", Nom=" + Nom + ", Prenom=" + Prenom + ", Cin=" + Cin + ", EmailProf="
				+ EmailProf + ", EmailPerso=" + EmailPerso + ", Password=" + Password + ", Tel=" + Tel + ", Autorise="
				+ Autorise + ", FichePfe=" + FichePfe + ", FormulaireConvention=" + FormulaireConvention + ", classe="
				+ classe + "]";
	}
	public Etudiant(String nom, String prenom, int cin, String emailProf, int tel,boolean depot) {
		super();
		Nom = nom;
		Prenom = prenom;
		Cin = cin;
		EmailProf = emailProf;
		Tel = tel;
		this.depot=depot;
	}

	
	
	
	
	
	
	
	
	

	
	
}
