package tn.esprit.ProjetStagePFE.ProjetStagePFE.util;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.*;
import javax.annotation.*;
import javax.ejb.*;
import javax.faces.bean.ViewScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.PIPFE.Entities.Ecole;
import tn.esprit.PIPFE.Entities.Enseignant;
import tn.esprit.PIPFE.Entities.Etudiant;
import tn.esprit.PIPFE.Entities.FichePFE;
import tn.esprit.PIPFE.Entities.HeadOfDepartment;
import tn.esprit.PIPFE.Services.CrudLocal;
import tn.esprit.PIPFE.Services.CrudRemote;
import tn.esprit.PIPFE.Services.CrudServices;

@Stateless
@LocalBean
@ManagedBean
@ViewScoped
public class TraitementsPériodiques {
@EJB 
private CrudLocal c;
private int id =1 ;
  private DateFormat info = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
 
 // @Schedule(dayOfWeek="Mon-Fri", hour="8") 
  //  @Schedule(dayOfWeek="Mon-Fri", hour="8")
  
  @Schedule(dayOfWeek="Mon-Fri", hour="8") 
   public void notification() throws NamingException {
	  ArrayList<Etudiant> et=(ArrayList<Etudiant>) c.getAllEtudiant();
		ArrayList<FichePFE> x=(ArrayList<FichePFE>) c.getAllfiche();
		ArrayList<Enseignant>ens=new ArrayList();
		SendingMail sm=new SendingMail("", "", "");
	
		
		for (Etudiant etudiant : et) {
			
			if(etudiant.getDepot()==true) {
				FichePFE f=etudiant.getFichePfe();
				
				if (f.getNoteEncadrant()==0) {
					
					ens.clear();
					ens.addAll(f.getEnseignant());
					if(ens.isEmpty()==false)
					{
					for (Enseignant en : ens) {
				if(en.getType_employe().matches("Encadrant")) {
							en.getEmail();
							sm.setContenu("Veuillez préciser une note pour l'étudiant "+etudiant.getNom());
							sm.setDestination(en.getEmail());
							sm.setSubject("note manquante");
							SendingMail.envoyer();}
						}
						
					}
					
				}
				
				if (f.getNoteRapporteur()==0) {
					ens.clear();
					ens.addAll(f.getEnseignant());
					
					for (Enseignant en : ens) {
				if(en.getType_employe().matches("Rapporteur")) {
							en.getEmail();
							sm.setContenu("Veuillez préciser une note pour l'étudiant "+etudiant.getNom());
							sm.setDestination(en.getEmail());
							sm.setSubject("note manquante");
							SendingMail.envoyer();
						}
						
					}
					
				}
				
				
				
			}
		}
	
		boolean test_rap;
		boolean test_enc;
		boolean test_pre;
		ArrayList<HeadOfDepartment> chefs=(ArrayList<HeadOfDepartment>) c.getAllchef();
		
		
		for(FichePFE f:x) { 
			 test_rap=false;
			 test_enc=false;
			 test_pre=true;
			
			 	if(f.getPrevalider()==false){
				test_pre=false;
							}
			 		ens.clear();
			 			ens.addAll(f.getEnseignant());
			 				for (Enseignant en : ens) {
			 						if(en.getType_employe().matches("Encadrant")) 
			 							test_enc=true;
			 						if(en.getType_employe().matches("Prevalidateur"))
			 							test_pre=true;
			 							
					
			 				}
			 				
			 				sm.setContenu("la fiche pfe de l'etudiant "+f.getEtudiant().getNom());
			 				if(test_rap==false)
			 					sm.setContenu(sm.getContenu()+" , necéssite un rapporteur");
			 				if(test_enc == false)
			 					sm.setContenu(sm.getContenu()+" , un encadrant");
			 				if(test_pre==false)
			 					sm.setContenu(sm.getContenu()+" , doit etre prévalidé");
			 				sm.setSubject("Rappel");
			 				for(HeadOfDepartment h:chefs) {
			 					sm.setDestination(h.getEmail());
			 					SendingMail.envoyer();
			 					
			 				}
			 				
			 				
				}
		
		
		
		
     
	   Logger.getLogger(TraitementsPériodiques.class.getName()).log(Level.INFO,"Execution du traitement toutes les 30 secondes "+info.format(new Date()));
   }
}