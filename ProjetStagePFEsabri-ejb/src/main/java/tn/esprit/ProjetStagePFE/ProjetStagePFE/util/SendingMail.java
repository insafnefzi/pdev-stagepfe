/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.ProjetStagePFE.ProjetStagePFE.util;

import java.util.Properties;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author SabriMarz
 */

@LocalBean
public class SendingMail {
    
      public static  String mailUsername ;
        public static  String mailPassword;
        public static String contenu ;
        public static String destination ;
        public static String subject ;
    public SendingMail(String contenu,String destination , String subject) {
        mailUsername = "sabri-b-m@live.fr";
        mailPassword = "sabrimarz1994";
        this.contenu=contenu ;
        this.destination=destination ;
        this.subject=subject ;
    }
        
        
public static void envoyer () {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.live.com");
        props.put("mail.smtp.port", "25");

        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
           @Override
           protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("sabri-b-m@live.fr", "sabrimarz1994");
            }
          });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("sabri-b-m@live.fr"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(destination));
            message.setSubject(subject);
            message.setText(contenu);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    
}


public static String getContenu() {
	return contenu;
}


public static void setContenu(String contenu) {
	SendingMail.contenu = contenu;
}


public static String getDestination() {
	return destination;
}


public static void setDestination(String destination) {
	SendingMail.destination = destination;
}


public static String getSubject() {
	return subject;
}


public static void setSubject(String subject) {
	SendingMail.subject = subject;
}

}